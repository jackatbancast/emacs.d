;;; init.el --- Init file

;;; Commentary:

;;; Code:

;; (require 'emacs-load-time)

(setq warning-minimum-level :emergency)
(defconst emacs-start-time (current-time))

(defvar file-name-handler-alist-old file-name-handler-alist)
(require 'package)
(setq package-enable-at-startup nil
      file-name-handler-alist nil
      message-log-max 16384
      gc-cons-threshold 402653184
      gc-cons-percentage 0.6
      auto-window-vscroll nil
      package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
			 ("melpa-stable" . "http://stable.melpa.org/packages/")
			 ("org-elpa" . "https://orgmode.org/elpa/")
			 ))

(add-hook 'after-init-hook
          `(lambda ()
             (setq file-name-handler-alist file-name-handler-alist-old
                   gc-cons-threshold 800000
                   gc-cons-percentage 0.1)
             (garbage-collect)) t)

(setq custom-file "~/.emacs.d/custom.el")
(load custom-file 'noerror)

(setq load-prefer-newer t)

;;; Functions

(eval-and-compile
  (defun emacs-path (path)
    (expand-file-name path user-emacs-directory)))

;; load directory for configuration files for emacs
(add-to-list 'load-path (emacs-path "setup-files/"))
(add-to-list 'load-path (emacs-path "lisp"))

(require 'setup-use-package)

(load (locate-user-emacs-file "general.el") nil :nomessage)

(setq org-roam-v2-ack t)

;; Load packages

(require 'setup-macos)
(require 'setup-auth-sources)
(require 'setup-which-key)
(require 'setup-holy)
(require 'setup-gpg)
(require 'setup-autocomplete)
(require 'setup-ivy)
(require 'setup-counsel)
(require 'setup-undo)
(require 'setup-snippets)
(require 'setup-eshell)
(require 'setup-git)
(require 'setup-org)
(require 'setup-modeline)
(require 'setup-macos)
(require 'setup-projectile)
(require 'setup-treemacs)
;; (require 'setup-python)
(require 'setup-webdev)
(require 'setup-golang)
(require 'setup-rust)
(require 'setup-docker)
(require 'setup-k8s)
(require 'setup-nix)
(require 'setup-winum)
(require 'setup-jump)
(require 'setup-avy)
(require 'setup-select)
(require 'setup-ops)
(require 'setup-general)
(require 'setup-audio)
(require 'setup-wm)
(require 'setup-http)
(require 'setup-translate)
(require 'setup-slack)
(require 'setup-jira)
(require 'setup-lsp)
(require 'setup-random)

(require 'notes)
(require 'pinentry-emacs)

(provide 'init)
;;; init.el ends here
(put 'downcase-region 'disabled nil)
