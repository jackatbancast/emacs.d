;;; setup-docker.el --- Set up docker interaction

;;; Commentary:

;;; Code:

(use-package dockerfile-mode
  :mode ("Dockerfile\\'" . dockerfile-mode))

(use-package docker
  :defer t)

(use-package docker-compose-mode
  :defer t)

(use-package tramp-container
  :ensure f
  :defer t)

(provide 'setup-docker)
;;; setup-docker.el ends here
