(use-package auth-source
  :ensure f
  :config
  (setq auth-sources '("~/.authinfo.gpg" "~/.authinfo" "~/.netrc")))

(provide 'setup-auth-sources)
