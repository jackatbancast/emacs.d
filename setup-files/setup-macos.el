;;; setup-macos.el --- Set up MacOS specifics

;;; Commentary:

;;; Code:

(use-package exec-path-from-shell
  :if (memq window-system '(mac ns))
  :ensure t
  :custom
  (exec-path-from-shell-variables
   (list
     "PATH"
     "MANPATH"
     "SSH_AUTH_SOCK"
     ))
  :config
  (exec-path-from-shell-initialize))

(use-package dired
  :ensure f
  :init
  (when (string= system-type "darwin")
    (setq dired-use-ls-dired nil)))

(use-package ef-themes
  :ensure t)

(use-package auto-dark
  :ensure t
  :config
  (setq auto-dark-dark-theme 'ef-duo-dark)
  (setq auto-dark-light-theme 'ef-duo-light)
  (setq auto-dark-polling-interval-seconds 5)
  (setq auto-dark-allow-osascript t)
  (setq auto-dark-allow-powershell nil)
  ;; (setq auto-dark-detection-method nil) ;; dangerous to be set manually

  ;; (add-hook 'auto-dark-dark-mode-hook
  ;;   (lambda ()
  ;;     ;; something to execute when dark mode is detected))

  ;; (add-hook 'auto-dark-light-mode-hook
  ;;   (lambda ()
  ;;     ;; something to execute when light mode is detected))

      (auto-dark-mode t))

(provide 'setup-macos)
;;; setup-macos.el ends here
