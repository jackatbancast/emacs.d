(use-package restclient
  :mode ("\\.http\\'" . restclient-mode))


(use-package company-restclient
  :after restclient)

(provide 'setup-http)
