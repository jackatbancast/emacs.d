;;; setup-projectile.el --- Set up projectile for project management

;;; Commentary:

;;; Code:

(use-package ripgrep
  :demand t)

(use-package projectile
  :bind-keymap ("M-p" . projectile-command-map)
  :bind
  ("C-c p p" . projectile-find-file)
  ("C-c p s" . projectile-ripgrep)
  ("C-c p '" . projectile-run-eshell)
  ("C-c p SPC" . projectile-switch-project)
  ("C-c p b" . projectile-switch-to-buffer)
  
  ("C-M-p" . projectile-find-file)
  ("C-M-s" . projectile-ripgrep)
  ("C-M-\"" . projectile-run-eshell)
  ("C-M-\)" . projectile-switch-project)
  ("C-M-b" . projectile-switch-to-buffer)
  :custom
  (projectile-enable-caching t)
  (projectile-completion-system 'ivy)
  :diminish)


(use-package counsel-projectile
  :after (counsel projectile)
  :config
  (counsel-projectile-mode 1)
)

(provide 'setup-projectile)
;;; setup-projectile.el ends here
