(use-package slack
  :commands (slack-start)
  :if (file-exists-p (emacs-path "custom-slack.el"))
  :init
  (setq slack-buffer-emojify t) ;; if you want to enable emoji, default nil
  (setq slack-prefer-current-team t)
  :config
  (load (emacs-path "custom-slack.el"))
  )

(use-package alert
  :commands (alert)
  :init
  (setq alert-default-style 'notifier))

(provide 'setup-slack)
