;;; setup-golang.el --- Set up golang things

;;; Commentary:

;;; Code:

(use-package go-mode
  :hook (go-mode . (lambda ()
		     (lsp)
		     (lsp-ui-mode)
		     (lsp-ui-sideline-mode)
		     (lsp-ui-doc-mode)
		     (company-mode))
		 )
  :custom
  (gofmt-command "goimports")
  :config
  (add-hook 'before-save-hook #'gofmt-before-save)
  )


;; (use-package go-eldoc
;;   :hook (go-mode . go-eldoc-setup)
;;   )

(use-package flycheck-golangci-lint
   :after flycheck
   :hook (flycheck-mode . flycheck-golangci-lint-setup))


(provide 'setup-golang)
;;; setup-golang.el ends here
