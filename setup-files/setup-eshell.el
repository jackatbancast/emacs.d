;;; setup-eshell.el -*- lexical-binding: t; -*-

;; Copyright (C) 2019 Jack Stephenson
;; Author: Jack Stephenson <jack@bancast.net>

;; eshell config
(use-package eshell
  :ensure nil
  :config

  (add-hook 'eshell-mode-hook
            (lambda ()
              (bind-keys
               :map eshell-mode-map
               ("<tab>" . completion-at-point)
               ("C-c M-o" . eshell-clear-buffer))))

  (defun eshell-clear-buffer ()
    "Clear terminal"
    (interactive)
    (let ((inhibit-read-only t))
      (erase-buffer)
      (eshell-send-input))))

(provide 'setup-eshell)
