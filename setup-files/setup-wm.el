;;; setup-wm.el --- Set up Window manager
;;; Commentary:

;;; Code:

(use-package i3wm
  :if (string= system-type "gnu/linux")
  )

(provide 'setup-wm)
;;; setup-wm.el ends here
