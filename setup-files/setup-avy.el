(use-package avy
  :bind
  ("C-:" . avy-goto-char)
  ("C-'" . avy-goto-char-2)
  ("M-g g" . avy-goto-line)
  ("M-g w" . avy-goto-word-1)
  :config
  (avy-setup-default)
  )

(provide 'setup-avy)
