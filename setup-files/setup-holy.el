;;; setup-holy.el --- Set up ESC binding

;;; Commentary:

;;; Code:

(use-package god-mode)

(provide 'setup-holy)
;;; setup-holy.el ends here
