;;; setup-flycheck.el --- Set up flycheck, for linting

;;; Commentary:

;;; Code:

(use-package flycheck
  :hook (org-mode . flycheck-mode)
  :config
  (global-flycheck-mode))

(provide 'setup-flycheck)
;;; setup-flycheck.el ends here
