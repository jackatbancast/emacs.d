(use-package undo-tree
  :config
  (global-undo-tree-mode +1)
  :diminish
  :demand
  :custom
  (undo-tree-history-directory-alist '(("." . "~/.undo-tree/")))
  (undo-tree-visualizer-diff t)
  :bind
  ("C-M-u" . undo-tree-visualize)
  )

(provide 'setup-undo)
