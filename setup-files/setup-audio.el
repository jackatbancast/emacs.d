;;; setup-audio.el --- Set up Audio

;;; Commentary:

;;; Code:

(use-package pulseaudio-control
  :if (string= system-type "gnu/linux")
  )

(provide 'setup-audio)
;;; setup-audio.el ends here
