
(use-package swoop
  :commands (swoop swoop-multi)
  :bind
  ("C-c s" . swoop-multi)
  )

(use-package expand-region
  :bind
  ("C-x C-," . er/expand-region)
  )

(use-package multiple-cursors
  :bind
  ("C-S-c C-S-c" . mc/edit-lines)
  ("C->" . mc/mark-next-like-this)
  ("C-<" . mc/mark-previous-like-this)
  ("C-c C-<" . mc/mark-all-like-this)
  )

(use-package anzu
  :config
  (global-anzu-mode +1)
  :diminish anzu-mode
  )

(provide 'setup-select)
