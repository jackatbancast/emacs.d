;;; setup-macos.el --- Set up MacOS specifics

;;; Commentary:

;;; Code:

(use-package pinentry
  :config
  (pinentry-start)
  )

(use-package epa-file
  :ensure f
  :after pinentry
  :config
  (epa-file-enable)
  (setq epa-pinentry-mode 'loopback)
  )


(provide 'setup-gpg)
;;; setup-gpg.el ends here
