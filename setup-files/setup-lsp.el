;;; setup-lsp.el --- Set up LSP integration

;;; Commentary:

;;; Code:

(use-package lsp-mode
  :demand t
  :commands lsp
  :custom
  (lsp-auto-configure t)
  ;; (lsp-inhibit-message t)
  ;; (lsp-enable-snippet t)
  ;; (lsp-auto-guess-root t)
  ;; (lsp-prefer-flymake nil)
  )

(use-package lsp-ui
  :demand t
  :config
  (setq lsp-ui-flycheck-enable t)
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
  :hook
  (lsp-mode . lsp-ui-mode))

(provide 'setup-lsp)
;;; setup-lsp.el ends here
