(auto-image-file-mode 1)

(add-to-list 'default-frame-alist
             '(font . "Hack Nerd Font-14"))
(set-face-attribute 'default t :font "Hack Nerd Font-14" )
(set-face-attribute 'default nil :font "Hack Nerd Font-14" )
;; (ignore-errors
;;   (set-frame-font "Hack Nerd Font-18" nil t))

(use-package browse-kill-ring
  :commands (browse-kill-ring)
  )

;; (use-package centaur-tabs
;;   :demand
;;   :config
;;   (centaur-tabs-mode t)
;;   :custom
;;   (centaur-tabs-style "bar")
;;   (centaur-tabs-set-modified-marker t)
;;   :bind
;;   ("C-<prior>" . centaur-tabs-backward)
;;   ("C-<next>" . centaur-tabs-forward))

(use-package restart-emacs)

(use-package doom-themes
  :after (all-the-icons)
  :custom
  (doom-themes-enable-bold t)
  (doom-themes-enable-italic t)
  :config
  (load-theme 'doom-dracula t)
  (doom-themes-visual-bell-config)
  (doom-themes-org-config)
  )

;; (use-package catppuccin-theme
;;   :custom
;;   ;; (catppuccin-flavor 'mocha)
;;   (catppuccin-flavor 'macchiato)
;;   ;; (catppuccin-flavor 'frappe)
;;   ;; (catppuccin-flavor 'latte)
;;   :config
;;   (load-theme 'catppuccin t)
;;   )

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package flyspell
  :hook (prog-mode . flyspell-prog-mode))

(use-package smartparens
  :config (smartparens-global-mode 1)
  :diminish smartparens-mode)

(use-package smex)

(use-package volatile-highlights
  :config
  (volatile-highlights-mode t))

;; General langs
(use-package yaml-mode)
(use-package json-mode)

(use-package editorconfig
  :config
  (editorconfig-mode 1))

(use-package zoom-window
  :bind ("C-c z" . zoom-window-zoom)
  )

(use-package graphviz-dot-mode)

(use-package mermaid-mode)

(use-package writeroom-mode
  :custom
  (writeroom-width 160)
  :bind
  ("C-c m" . writeroom-mode)
  ("C-M-<" . writeroom-decrease-width)
  ("C-M->" . writeroom-increase-width)
  ("C-M-=" . writeroom-adjust-width)
  )

(use-package rainbow-mode
  :config
  (rainbow-mode t)
  )

(provide 'setup-general)
