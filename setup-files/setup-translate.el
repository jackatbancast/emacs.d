(use-package google-translate
  :commands (google-translate-at-point google-translate-query-translate google-translate-smooth-translate))

(provide 'setup-translate)
