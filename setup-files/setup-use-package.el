;;; setup-use-package.el --- Set up the package management

;;; Commentary:
;; This should come first in the init file

;;; Code:

(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-initialize)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(setq
 use-package-verbose t
 use-package-enable-imenu-support t)

(require 'use-package-ensure)
(setq use-package-always-ensure t)

(use-package diminish
  :ensure t
  :config (diminish eldoc-mode)
  )

(use-package use-package-ensure-system-package
  :ensure t)

(use-package quelpa-use-package
  :ensure t)

(use-package auto-package-update
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
  (auto-package-update-maybe))

(provide 'setup-use-package)
;;; setup-use-package.el ends here
