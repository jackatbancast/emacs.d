(use-package dumb-jump
  ;; :disabled
  :bind (("C-c j" . dumb-jump-go-prompt))
  :config (setq dumb-jump-selector 'ivy))

(use-package ace-jump-mode
  :bind
  ("C-c SPC" . ace-jump-char-mode)
  ("C-c C-j" . ace-jump-char-mode)
  ("C-M-s-j" . ace-jump-char-mode)
  )

(provide 'setup-jump)
