;;; setup-dap.el --- Set up DAP

;;; Commentary:

;;; Code:

(use-package dap-mode
  )

(use-package dap-go
  :ensure f
  :after dap-mode
  :config
  (dap-go-setup)
  )


(provide 'setup-dap)
;;; setup-dap.el ends here
