;;; setup-treemacs.el --- Set up treemacs, for folder browsing

;;; Commentary:

;;; Code:


(use-package treemacs
  :custom
  (treemacs-git-integration t)
  ;; (treemacs-no-png-images t)
  ;; (treemacs-width 35)
  (treemacs-display-in-side-window t)
  ;; (treemacs-indentation-string (propertize " " 'face 'font-lock-comment-face))
  ;; (treemacs-indentation 1)
  :config
  (treemacs-git-mode 'deferred)
  (treemacs-filewatch-mode)
  :bind ("C-c t" . treemacs)
  :bind ("C-M-t" . treemacs)
  )

(use-package treemacs-all-the-icons
  :after (treemacs all-the-icons)
  :config
  (treemacs-load-theme "all-the-icons")
  )

(use-package treemacs-magit
  :after (treemacs))

(use-package treemacs-projectile
  :after (projectile treemacs)
  )

;; (use-package lsp-treemacs
;;   :after (treemacs lsp-mode))


(provide 'setup-treemacs)
