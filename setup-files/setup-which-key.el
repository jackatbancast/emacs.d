;;; setup-which-key.el --- Everyone likes to see what they're doing

;;; Commentary:

;;; Code:

(use-package which-key
  :config (which-key-mode 1))

(provide 'setup-which-key)
;;; setup-which-key.el ends here
