;;; setup-ops.el --- Set up things to do with ops

;;; Commentary:

;;; Code:

;; (use-package hcl-mode
;;   :mode ("\\.tf" . hcl-mode))

;; Terraform syntax highlighting
(use-package terraform-mode)
(use-package terraform-doc)
(use-package company-terraform)

(use-package just-mode)

(use-package chef-mode)

(use-package bazel)

(use-package jsonnet-mode
  :config
  (defun jsonnet-reformat-buffer ()
    "Reformat entire buffer using the Jsonnet format utility."
    (interactive)
    (call-process-region (point-min) (point-max) "jsonnetfmt" t t nil "-"))
  )

(use-package rbenv
  :config
  (global-rbenv-mode)
  )

(provide 'setup-ops)
;;; setup-ops.el ends here
