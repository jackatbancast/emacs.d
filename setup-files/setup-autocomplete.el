;;; setup-autocomplete.el --- Set up autocomplete

;;; Commentary:

;;; Code:

(use-package company
  :bind
  ("C-." . company-complete)
  :demand
  :diminish company-mode
  :custom
  (company-idle-delay 5 "Instant help can be distracting")
  (company-dabbrev-downcase nil "Don't downcase returned candidates.")
  (company-show-numbers t "Numbers are helpful.")
  (company-tooltip-limit 20 "The more the merrier.")
  (company-abort-manual-when-too-short t "Be less enthusiastic about completion.")
  :config
  (global-company-mode +1)
  )

(use-package company-quickhelp
  :after company
  :config (company-quickhelp-mode)
  :diminish company-quickhelp-mode)

(use-package company-tabnine
  :after company
  ;; :config
  ;; (cond
  ;;  ((not (member #'company-tabnine company-backends))
  ;;   (add-to-list 'company-backends #'company-tabnine)))
  )

(provide 'setup-autocomplete)
;;; setup-autocomplete.el ends here
