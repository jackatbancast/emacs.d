;;; setup-python.el --- Set up things to do with Python

;;; Commentary:

;;; Code:

(use-package python-mode
  :hook (python-mode . (lambda ()
		     (lsp)
		     (lsp-ui-mode)
		     (lsp-ui-sideline-mode)
		     (lsp-ui-doc-mode)
		     (company-mode))
		 )
  )
