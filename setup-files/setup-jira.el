(use-package org-jira
  :if (file-exists-p (emacs-path "custom-jira.el"))
  :config
  (load (emacs-path "custom-jira.el"))
  )

(provide 'setup-jira)
