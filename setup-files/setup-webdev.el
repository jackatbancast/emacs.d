;;; setup-webdev.el --- Set up things to do with Python

;;; Commentary:

;;; Code:

(use-package tide
  :hook (tide-mode . tide-format-before-save)
  )


(provide 'setup-webdev)
