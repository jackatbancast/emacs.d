;;; setup-git.el --- Set up Git things (magit)

;;; Commentary:

;;; Code:

(use-package magit
  ;; :after which-key
  :demand t
  :bind
  ("C-c g s" . magit-status)
  ("C-M-g" . magit-status)
  :config
  ;; Disable git from vc handled backends. magit is better suited to handle this
  (setq vc-handled-backends (delq 'Git vc-handled-backends))
  :custom
  (magit-refresh-status-buffer nil)
  )

(use-package git-timemachine
  :bind ("C-c g t" . git-timemachine)
  )

(use-package forge
  :after magit)

;; (use-package dired-git
;;   :if (not (memq window-system '(mac ns)))
;;   :hook (dired-mode . dired-git-mode))

(provide 'setup-git)
;;; setup-git.el ends here
