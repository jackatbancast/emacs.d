(defun air-org-skip-subtree-if-priority (priority)
  "Skip an agenda subtree if it has a priority of PRIORITY.

PRIORITY may be one of the characters ?A, ?B, or ?C."
  (let ((subtree-end (save-excursion (org-end-of-subtree t)))
        (pri-value (* 1000 (- org-lowest-priority priority)))
        (pri-current (org-get-priority (thing-at-point 'line t))))
    (if (= pri-value pri-current)
        subtree-end
      nil)))


(defun air-org-skip-subtree-if-habit ()
  "Skip an agenda entry if it has a STYLE property equal to \"habit\"."
  (let ((subtree-end (save-excursion (org-end-of-subtree t))))
    (if (string= (org-entry-get nil "STYLE") "habit")
        subtree-end
      nil)))


(use-package plantuml-mode
  :mode "\\.puml\\'"
  :mode "\\.iuml\\'"
  :mode "\\.plantuml\\'"
  :custom
  (plantuml-jar-path (emacs-path "plantuml.jar") "Path to plantUML jar")
  (plantuml-default-exec-mode (quote jar) "Default execution mode")
  )

(use-package org
  ;; Always get this from the GNU archive.
  :pin gnu
  :diminish org-indent-mode
  :defer t
  :bind (("\C-c a" . org-agenda)
	 ("\C-c c" . org-capture)
	 )
  :hook
  (org-mode . visual-line-mode)
  (org-mode . display-line-numbers-mode)
  (org-mode . flyspell-mode)
  :config
  (setq org-todo-keywords
	'((sequence "TODO(t)" "NEXT(n)" "WAITING(w)" "|" "DONE(d!)" "WONTFIX(k!)")
	  (sequence "SCHEDULED(s@)" "|" "MEETING(m@/!)" "CANCELLED(c@)")
	  (sequence "|" "DELEGATED(o@/!)"))
	)

  (setq org-fast-tag-selection-single-key 'expert)

  (setq org-use-fast-todo-selection t)
  (setq org-fast-tag-selection-include-todo t)

  (org-babel-do-load-languages 'org-babel-load-languages
    '(
        (shell . t)
        (dot . t)
        (plantuml . t)
	))

  (setq org-capture-templates
	'(
	  ("p" "Protocol" entry (file+headline "~/notes/protocol.org" "Inbox")
           "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")
	  ("L" "Protocol Link" entry (file+headline "~/notes/protocol.org" "Inbox")
           "* %? [[%:link][%:description]] \nCaptured On: %U")
	  ("t" "Todo" entry (file+headline "~/notes/general.org" "Tasks")
	   "* TODO %?\t%^G\n  DEADLINE %^t\n  %a")
	  ("j" "Journal" entry (file+datetree "~/notes/journal.org")
	   "* %?\t%^G\n  Entered on %U\n  %a")
	  ("m" "Schedule a meeting" entry (file+headline "~/notes/general.org" "Meetings")
	   "* SCHEDULED Meeting with %^{People} about %^{Subject}\n  DEADLINE\: %^T\t%^G\n  Agenda: %?")
	  ))

  (setq org-directory "~/notes")
  (setq org-default-notes-file "~/notes/refile.org")
  (defvar org-default-diary-file "~/notes/journal.org")
  (setq org-agenda-files
	(directory-files-recursively
	 "~/notes" "\\.org$" t
	 ;; "~/notes" "^[^.]+$" t
	 (lambda (x)
	   "Filter out archive"
	   (not
	     (string= x (expand-file-name "archive" org-directory))
	     ))))

  ;; (setq org-cycle-separator-lines 0)
  (setq org-tags-column 80)
  (setq org-agenda-tags-column org-tags-column)
  (setq org-agenda-sticky t)

  (setq org-agenda-custom-commands
	'(("d" "Daily agenda and all TODOs"
	   ((tags "PRIORITY=\"A\""
		  ((org-agenda-skip-function '(org-agenda-skip-entry-if 'done)) ;; 'todo 'done
		   (org-agenda-overriding-header "High-priority unfinished tasks:")))
	    (agenda "" ((org-agenda-ndays 2)))
	    (alltodo ""
		     ((org-agenda-skip-function '(or (air-org-skip-subtree-if-habit)
						     (air-org-skip-subtree-if-priority ?A)
						     (org-agenda-skip-if nil '(scheduled deadline))))
		      (org-agenda-overriding-header "ALL normal priority tasks:"))))
	   ((org-agenda-compact-blocks t)))))


  (setq org-refile-targets (quote ((nil :maxlevel . 9)
				   (org-agenda-files :maxlevel . 9)
				   ;; ((lambda () "" (directory-files-recursively
				   ;;  "~/notes" "\\.org$" nil
				   ;;  (lambda (x)
				   ;;    "Filter only dailies"
				   ;; 	(string= x (expand-file-name "daily" org-directory)))) :maxlevel . 1))
				   ;; ((directory-files-recursively
				   ;;  "~/notes" "\\.org$" nil
				   ;;  (lambda (x)
				   ;;    "Filter out archive and dailies"
				   ;;    (not
				   ;;     (or
				   ;; 	(string= x (expand-file-name "archive" org-directory))
				   ;; 	(string= x (expand-file-name "daily" org-directory)))))) :maxlevel . 3)
				   ;; ((directory-files-recursively
				   ;;  "~/notes" "\\.org$" nil
				   ;;  (lambda (x)
				   ;;    "Filter only dailies"
				   ;; 	(string= x (expand-file-name "daily" org-directory)))) :maxlevel . 1)
				   )))

  (setq org-refile-use-outline-path t)
  (setq org-outline-path-complete-in-steps nil)
  (setq org-refile-allow-creating-parent-nodes 'confirm)

  (setq org-archive-location "archive/%s_archive::")
  (defvar org-archive-file-header-format "#+FILETAGS: ARCHIVE\nArchived entries from file %s\n")

  (setq org-src-fontify-natively t)
  (setq org-display-inline-images t)
  (setq org-redisplay-inline-images t)
  (setq org-startup-with-inline-images t)

  (setq org-plantuml-jar-path
	(emacs-path "plantuml.jar"))
)

(use-package org-protocol
  :ensure f)

(use-package org-habit
  :after (org)
  :ensure f
  :custom
  (org-habit-show-habits-only-for-today nil)
  (org-habit-graph-column 80))

;;; Org-babel
(use-package ob-async)
(use-package ob-http)
(use-package ob-python
  :ensure f)
(use-package ob-mermaid)
(use-package ob-go)
(use-package ob-dot
  :ensure f)
;; (use-package ob-rust)
;; (use-package org-babel
;;   :ensure f
;;   :after (org ob-python ob-go ob-dot ob-rust)
;;   :config
;;   (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '(
;;    (python . t)
;;    (go . t)
;;    (dot . t)
;;    (rust . t)
;;    ))
;;   )
(use-package ob-restclient
  :after restclient)

(use-package ox-jira
  :after org
  )

(use-package oer-reveal
  :after org)

(use-package ox-reveal
  :after org)

(use-package ox-pandoc
  :after org
  )

(use-package org-download
  :after org
  :bind
  (:map org-mode-map
        (("C-c D Y" . org-download-screenshot)
         ("C-c D y" . org-download-yank))))

(use-package org-roam
      :ensure t
      :custom
      (org-roam-directory (expand-file-name "notes/roam" "~/"))
      (org-roam-graph-executable "neato")
      :init
      (setq org-roam-v2-ack t)
      (setq org-roam-dailies-directory "daily/")
      (setq org-roam-dailies-capture-templates
	    '(("d" "default" entry
               "* %?"
               :target (file+head "%<%Y-%m-%d>.org"
				  "#+title: %<%Y-%m-%d>\n"))))
      :bind (("C-c n l" . org-roam-buffer-toggle)
             ("C-c n f" . org-roam-node-find)
             ("C-c n g" . org-roam-graph)
             ("C-c n i" . org-roam-node-insert)
             ("C-c n c" . org-roam-capture)
             ;; Dailies
             ("C-c n j" . org-roam-dailies-capture-today)
	     ("C-M-r" . org-roam-dailies-goto-today)
	     ("C-c C-r t" . org-roam-dailies-goto-today)
	     ("C-c C-r n" . org-roam-dailies-goto-tomorrow)
	     ("C-c C-r p" . org-roam-dailies-goto-yesterday)
	     ("C-c C-r c" . org-roam-dailies-capture-today)
	     )
      :config
      (org-roam-setup)
      ;; If using org-roam-protocol
      (require 'org-roam-protocol))

;; (use-package websocket
;;     :after org-roam)

(use-package org-roam-ui
  :after org-roam ;; or :after org
  :hook (org-roam-mode . org-roam-ui-mode)
  ;; :quelpa (
  ;; 	   org-roam-ui
  ;; 	   :fetcher github
  ;; 	   :repo "org-roam/org-roam-ui"
  ;; 	   )
  )

;; Setup from https://colekillian.com/posts/org-pomodoro-and-polybar/
(use-package org-pomodoro
  :commands (org-pomodoro)
  :config
  (setq
   alert-user-configuration (quote ((((:category . "org-pomodoro")) libnotify nil)))
   )
  )

(defun ruborcalor/org-pomodoro-time ()
  "Return the remaining pomodoro time"
  (if (org-pomodoro-active-p)
      (cl-case org-pomodoro-state
        (:pomodoro
           (format "Pomo: %d minutes - %s" (/ (org-pomodoro-remaining-seconds) 60) org-clock-heading))
        (:short-break
         (format "Short break time: %d minutes" (/ (org-pomodoro-remaining-seconds) 60)))
        (:long-break
         (format "Long break time: %d minutes" (/ (org-pomodoro-remaining-seconds) 60)))
        (:overtime
         (format "Overtime! %d minutes" (/ (org-pomodoro-remaining-seconds) 60))))
    "No active pomo"))


(provide 'setup-org)
;;; setup-org.el ends here
