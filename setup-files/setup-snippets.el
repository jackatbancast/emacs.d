;;; setup-snippets.el --- Set up snippets here

;;; Commentary:

;;; Code:

;; (defun company-mode/backend-with-yas (backend)
;;   (if (and (listp backend) (member 'company-yasnippet backend))
;;        backend
;;        (append (if (consp backend) backend (list backend))
;; 	       '(:with company-yasnippet))))

(use-package yasnippet
  :diminish yas-minor-mode
  :after company
  ;; :mode ("/\\.emacs\\.d/snippets/" . snippet-mode)
  :config
  (yas-global-mode 1)
  ;; (setq
  ;;  yas-prompt-functions '(yas-completing-prompt))
  (add-to-list 'yas-snippet-dirs (emacs-path "snippets"))
  ;; (cond
  ;;  ((not (member #'company-yasnippet company-backends))
  ;;   (add-to-list 'company-backends #'company-yasnippet)))
  ;; (setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))
  )

(use-package yasnippet-snippets
  :after yasnippet)

(provide 'setup-snippets)
;;; setup-snippets.el ends here
