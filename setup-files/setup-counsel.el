(use-package counsel
  :ensure t
  :after (ivy)
  :config
  (counsel-mode +1)
  (defun counsel-rg-at-point ()
    (interactive)
    (let ((selection (thing-at-point 'word)))
      (if (<= 4 (length selection))
          (counsel-rg selection)
        (counsel-rg))))

  :diminish)

(provide 'setup-counsel)
