;;; setup-modeline.el --- Set up modeline

;;; Commentary:

;;; Code:

(use-package all-the-icons)
(use-package doom-modeline
  :after all-the-icons
  :hook (after-init . doom-modeline-mode))


(use-package all-the-icons-dired
  :after (all-the-icons)
  :hook (dired-mode . all-the-icons-dired-mode)
  )

(provide 'setup-modeline)
;;; setup-modeline.el ends here
