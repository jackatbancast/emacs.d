
(use-package ivy
  :ensure t
  :init
  (ivy-mode +1)
  :custom
  (ivy-height 30)
  (ivy-use-virtual-buffers t)
  (ivy-use-selectable-prompt t)
  :diminish
  )

(use-package ivy-rich
  :after counsel
  :custom
  (ivy-virtual-abbreviate 'full
			  ivy-rich-switch-buffer-align-virtual-buffer t
			  ivy-rich-path-style 'abbrev)
  :init
  (ivy-rich-mode 1))

(use-package all-the-icons-ivy-rich
  :after (ivy-rich)
  :init (all-the-icons-ivy-rich-mode 1)
  )

(provide 'setup-ivy)
