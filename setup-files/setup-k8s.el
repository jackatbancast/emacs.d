;;; setup-k8s.el --- Set up things to do with kubernetes

;;; Commentary:

;;; Code:
;; (use-package kubel)

(use-package kubernetes
  :ensure t
  :commands (kubernetes-overview))

;; (use-package kubernetes-tramp
;;   :defer t
;;   :custom
;;   (tramp-remote-shell-executable "sh"))

(provide 'setup-k8s)
;;; setup-k8s.el ends here
