(defconst notes-default-path
  (expand-file-name "notes" "~/")
  "Default-location of notes on the system")

(defconst notes-default-extension
  "org"
  "Default extension for notes files")

(defun notes-path
    (note) "Get the note file for the "
    (expand-file-name note notes-default-path))

(defun projectile-project-notes
    () "Open the notes file corresponding to the current project"
    (interactive)
    (find-file
     (notes-path
      (concat (projectile-project-name) "." notes-default-extension))))

(provide 'notes)
