;;; general.el --- General config

;;; Commentary:

;;; Code:


(eval-and-compile
  (setq backup-by-copying t      ; don't clobber symlinks
	backup-directory-alist '(("." . "~/.saves"))    ; don't litter my fs tree
	delete-old-versions t
	kept-new-versions 6
	kept-old-versions 2
	version-control t
	auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
	create-lockfiles nil
	)

  (when (version<= "26.0.50" emacs-version )
    (add-hook 'prog-mode-hook 'display-line-numbers-mode)
    (add-hook 'jsonnet-mode-hook 'display-line-numbers-mode)
    )

  (subword-mode)
  
  ;; Remove annoying toolbar for UI
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1)
  ;; (fringe-mode -1)

  (global-hl-line-mode t)              ; Always highlight the current line.
  (show-paren-mode t)                  ; And point out matching parentheses.
  (delete-selection-mode t)            ; Behave like any other sensible text editor would.

  (prefer-coding-system 'utf-8)
  (setq-default buffer-file-coding-system 'utf-8-auto-unix)

  (fset 'yes-or-no-p 'y-or-n-p)
  (setq initial-major-mode 'text-mode
	initial-scratch-message nil
	inhibit-startup-screen t
	)

  ;; (server-start)
  )


(provide 'general)
;;; general.el ends here
